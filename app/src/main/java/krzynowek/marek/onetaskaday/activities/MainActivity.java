package krzynowek.marek.onetaskaday.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.crashlytics.android.Crashlytics;

import net.danlew.android.joda.JodaTimeAndroid;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import krzynowek.marek.onetaskaday.R;
import krzynowek.marek.onetaskaday.fragments.HomeFragment;

public class MainActivity extends AppCompatActivity {

  @BindView(R.id.fragment_container) View container;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    JodaTimeAndroid.init(this);
    Fabric.with(this, new Crashlytics());

    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    if (savedInstanceState == null) {
      this.setInitialFragment();
    }
  }

  private void setInitialFragment(){
    HomeFragment initialFragment = new HomeFragment();
    getSupportFragmentManager().beginTransaction()
        .add(R.id.fragment_container, initialFragment).commit();
  }
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
