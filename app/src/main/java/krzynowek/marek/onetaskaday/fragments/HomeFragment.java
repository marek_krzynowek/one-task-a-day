package krzynowek.marek.onetaskaday.fragments;

import android.content.DialogInterface;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.seismic.ShakeDetector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import krzynowek.marek.onetaskaday.R;
import krzynowek.marek.onetaskaday.helpers.DialogModule;
import krzynowek.marek.onetaskaday.helpers.OnSwipeListener;
import krzynowek.marek.onetaskaday.model.Task;
import krzynowek.marek.onetaskaday.presenters.HomePresenter;
import krzynowek.marek.onetaskaday.presenters.HomeView;

import static android.content.Context.SENSOR_SERVICE;

public class HomeFragment extends Fragment implements ShakeDetector.Listener, HomeView {

  @BindView(R.id.task_display) TextView taskDisplay;
  @BindView(R.id.transparentOverlay) View overlay;
  @BindView(R.id.touch_surface) View touchSurface;
  @BindView(R.id.task_list) View bottomSheet;

  private @NonNull ShakeDetector sd = new ShakeDetector(this);
  private @Nullable SensorManager sensorManager;
  private @NonNull TaskListFragment taskListFragment = new TaskListFragment();

  private HomePresenter presenter;

  public HomeFragment() {}

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    presenter = new HomePresenter(this);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_main, container, false);
    ButterKnife.bind(this, view);
    presenter.onCreateView();
    BottomSheetBehavior.from(bottomSheet).setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
      @Override
      public void onStateChanged(@NonNull View bottomSheet, int newState) {
        if(newState == BottomSheetBehavior.STATE_COLLAPSED) {
          presenter.showCurrentTask();
        }
      }

      @Override
      public void onSlide(@NonNull View bottomSheet, float slideOffset) {
      }
    });
    return view;
  }

  public void initTapDetector() {
    touchSurface.setOnTouchListener(new OnSwipeListener(presenter));
  }

  @Override
  public void onStart() {
    super.onStart();
    taskListFragment.refreshList();
  }

  public void addTaskList() {
    taskListFragment = new TaskListFragment();
      getChildFragmentManager().beginTransaction()
          .replace(R.id.task_list_container, taskListFragment)
          .commit();
  }

  @Override
  public void showOverlay() {
    overlay.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideOverlay() {
    overlay.setVisibility(View.INVISIBLE);
  }

  public void showTask(@Nullable Task task){
    if( task != null) {
      taskDisplay.setText(task.toString());
    } else {
      taskDisplay.setText(R.string.add_some_tasks);
    }
  }

  public void refreshList() {
    taskListFragment.refreshList();
  }

  @Override
  public void startShakeDetector() {
    if (sensorManager == null) {
      sensorManager = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);
    }
    sd.start(sensorManager);
  }

  @Override
  public void stopShakeDetector() {
    sd.stop();
  }

  @OnClick(R.id.fab)
  public void showAddNewTask(View view) {
    AddTaskFragment addTaskFragment = new AddTaskFragment();
    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
    transaction.replace(R.id.fragment_container, addTaskFragment);
    transaction.addToBackStack(null);
    transaction.commit();
  }

  @Override
  public void showDoneDialog() {
    DialogModule.showTaskDoneDialog(getActivity(), presenter);
  }

  @Override
  public void hearShake() {
    presenter.draw();
    sd.stop();
    //wait before detecting shakes again
    //TODO: display cooldown progress bar to inform shake is not available
    new CountDownTimer(2000, 2000) {
      public void onTick(long millisUntilFinished) {}
      public void onFinish() {
        if (sensorManager != null) {
          sd.start(sensorManager);
        }
      }
    }.start();
  }
}
