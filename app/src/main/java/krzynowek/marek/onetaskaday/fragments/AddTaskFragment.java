package krzynowek.marek.onetaskaday.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import krzynowek.marek.onetaskaday.R;
import krzynowek.marek.onetaskaday.model.Task;

/**
 * A simple {@link Fragment} subclass.
 */

public class AddTaskFragment extends Fragment {

  @BindView(R.id.task_content) EditText taskContent;
  @BindView(R.id.done_button) Button doneButton;
  @BindView(R.id.next_button) Button nextButton;

  public AddTaskFragment() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_add_task, container, false);
    ButterKnife.bind(this, view);
    tryEnableButtons();
    taskContent.addTextChangedListener(new TextWatcher() {
      @Override
      public void afterTextChanged(Editable arg0) {
        tryEnableButtons();
      }

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }
    });
    return view;
  }

  @Override
  public void onStart() {
    super.onStart();
    becomeFirstResponder(taskContent);
  }

  @Override
  public void onStop() {
    super.onStop();
    resignFirstResponder(taskContent);
  }

  public void resignFirstResponder(EditText et) {
    et.clearFocus();
    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromInputMethod(et.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
  }

  private void becomeFirstResponder(EditText et){
    et.requestFocus();
    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
  }

  private void tryEnableButtons() {
    boolean hasText = !taskContent.getText().toString().trim().isEmpty();
    doneButton.setEnabled(hasText);
    nextButton.setEnabled(hasText);
  }


  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
  }

  @Override
  public void onDetach() {
    super.onDetach();
  }

  @OnClick(R.id.done_button)
  public void done() {
    saveTask();
    getFragmentManager().popBackStack();
  }

  @OnClick(R.id.next_button)
  public void addMore() {
    saveTask();
    taskContent.setText("");
  }

  private void saveTask() {
    String content = taskContent.getText().toString().trim();
    if(!content.isEmpty()) {
      Task task = new Task(content);
      task.save();
    }
  }
}
