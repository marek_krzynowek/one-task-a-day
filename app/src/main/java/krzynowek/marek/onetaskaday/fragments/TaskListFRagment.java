package krzynowek.marek.onetaskaday.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import krzynowek.marek.onetaskaday.R;
import krzynowek.marek.onetaskaday.adapters.TaskListAdapter;
import krzynowek.marek.onetaskaday.helpers.ExpandAnimation;
import krzynowek.marek.onetaskaday.model.Task;

/**
 * Created by Marek Krzynowek on 12/15/16.
 */

public class TaskListFragment extends Fragment {

  @BindView(R.id.tasks_list_view) ListView taskList;

  private @NonNull List<Task> tasks = new ArrayList<>();
  private @Nullable TaskListAdapter adapter;

  public TaskListFragment() {}

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_task_list, container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  private void setAdapter() {
    adapter = new TaskListAdapter(this, tasks);
    taskList.setAdapter(adapter);
  }

  public void refreshList(){
    tasks.clear();
    tasks.addAll(Task.getTodos());
    if(adapter == null) {
      setAdapter();
    }
    adapter.notifyDataSetChanged();
  }
}
