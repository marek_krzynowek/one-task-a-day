package krzynowek.marek.onetaskaday.presenters;

import krzynowek.marek.onetaskaday.helpers.DialogModule;
import krzynowek.marek.onetaskaday.model.Task;

/**
 * Created by Marek Krzynowek on 12/21/16.
 */

public interface HomeView {

  void showTask(Task task);
  void refreshList();
  void startShakeDetector();
  void stopShakeDetector();
  void initTapDetector();
  void addTaskList();
  void showOverlay();
  void hideOverlay();
  void showDoneDialog();
}
