package krzynowek.marek.onetaskaday.presenters;

import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import krzynowek.marek.onetaskaday.helpers.DialogModule;
import krzynowek.marek.onetaskaday.helpers.OnSwipeListener;
import krzynowek.marek.onetaskaday.model.Task;
import krzynowek.marek.onetaskaday.services.TaskLotteryService;

import static krzynowek.marek.onetaskaday.helpers.OnSwipeListener.LEFT;

/**
 * Created by Marek Krzynowek on 12/21/16.
 */

public class HomePresenter implements OnSwipeListener.OnSwipeDelegate, DialogModule.DialogDelegate {

  private @NonNull HomeView view;
  private @Nullable Task currentTask;

  public HomePresenter(@NonNull HomeView view){
    this.view = view;
    setCurrentTask();
    view.startShakeDetector();
  }
  public void onCreateView(){
    showCurrentTask();
    view.initTapDetector();
    view.addTaskList();
  }

  private void setCurrentTask() {
    currentTask = Task.getCurrent();
    if (currentTask == null) {
      currentTask = TaskLotteryService.draw();
    }
  }

  public void showCurrentTask() {
    setCurrentTask();
    view.showTask(currentTask);
  }

  public void draw() {
    currentTask = TaskLotteryService.draw();
    view.showTask(currentTask);
  }

  public void shakeDetected() {
    draw();
    view.stopShakeDetector();
    //wait before detecting shakes again
    //TODO: display cooldown progress bar to inform shake is not available
    new CountDownTimer(2000, 2000) {
      public void onTick(long millisUntilFinished) {}

      public void onFinish() {
        view.startShakeDetector();
      }
    }.start();
  }

  @Override
  public void confirmTapped() {
    if (currentTask != null) {
      currentTask.complete();
      draw();
      view.refreshList();
    }
  }

  @Override
  public void cancelTapped() {}// NO Action

  @Override
  public void onTouch(){
    view.showOverlay();
  }

  @Override
  public void onSwipe(@OnSwipeListener.Direction String direction) {
    if(direction.equals(LEFT)){
      draw();
    } else {
      view.showDoneDialog();
    }
  }

  @Override
  public void touchesDone() {
    view.hideOverlay();
  }
}
