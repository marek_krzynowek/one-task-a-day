package krzynowek.marek.onetaskaday.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Marek Krzynowek on 11/9/16.
 */

@Parcel(analyze = Task.class)
public class Task extends SugarRecord {
  private @NonNull String content = "";
  private boolean done = false;
  private @NonNull Date createdAt = new Date();
  private boolean current = false;

  public Task() {
    // Required for Sugar ORM
  }

  public Task(@NonNull String content) {
    this.content = content;
    this.createdAt = new Date();
  }

  public @NonNull String getContent() {
    return content;
  }

  public void setContent(@NonNull String content) {
    this.content = content;
  }

  public boolean isDone() {
    return done;
  }

  public boolean isCurrent() { return current; }

  public void setCurrent(boolean current) {
    this.current = current;
  }

  public void complete(){
    this.done = true;
    this.current = false;
    this.save();
  }

  //TODO implement this with prettytime library
  public String getAge(){
    return "5 days ago";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Task task = (Task) o;

    return content.equals(task.content);

  }

  @Override
  public int hashCode() {
    return content.hashCode();
  }

  @Override
  public String toString() {
    return content;
  }

  public static @NonNull List<Task> getTodos() {
    List<Task> todos = Select.from(Task.class)
        .where(Condition.prop("done").eq(0))
        .list();
    if( todos == null) {
      return new ArrayList<>();
    } else {
      return todos;
    }
  }

  public static @Nullable Task getCurrent() {
    return Select.from(Task.class)
        .where(Condition.prop("done").eq(0))
        .and(Condition.prop("current").eq(1)).first();
  }
}
