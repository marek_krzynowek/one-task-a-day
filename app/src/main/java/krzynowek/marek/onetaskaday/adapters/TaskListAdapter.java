package krzynowek.marek.onetaskaday.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import krzynowek.marek.onetaskaday.R;
import krzynowek.marek.onetaskaday.fragments.TaskListFragment;
import krzynowek.marek.onetaskaday.helpers.DialogModule;
import krzynowek.marek.onetaskaday.helpers.ExpandAnimation;
import krzynowek.marek.onetaskaday.model.Task;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Marek Krzynowek on 12/15/16.
 */

public class TaskListAdapter extends ArrayAdapter<Task> {

  private @NonNull TaskListFragment fragment;
  private @Nullable View visiblePanel;

  public TaskListAdapter(TaskListFragment fragment, List<Task> tasks) {
    super(fragment.getContext(), 0, tasks);
    this.fragment = fragment;
  }

  @NonNull
  @Override
  public View getView(int position, View convertView, @NonNull ViewGroup parent) {
    final Task task = getItem(position);
    final TaskViewHolder holder;
    if (convertView != null) {
      holder = (TaskViewHolder) convertView.getTag();
    } else {
      convertView = LayoutInflater.from(getContext()).inflate(R.layout.task_list_item, parent, false);
      holder = new TaskViewHolder(convertView);
      convertView.setTag(holder);
    }
    if (task != null) {
      holder.name.setText(task.getContent());
      holder.age.setText(task.getAge());
      configureListeners(convertView, task);
      hideButtons(convertView);
    }
    return convertView;
  }

  private void configureListeners(View convertView, Task task) {
    convertView.setOnClickListener(new ListRowOnClickListener());

    FancyButton doneButton = (FancyButton) convertView.findViewById(R.id.done_button);
    doneButton.setOnClickListener(new DoneButtonOnClickListener(fragment, task));

    FancyButton deleteButton = (FancyButton) convertView.findViewById(R.id.delete_button);
    deleteButton.setOnClickListener(new DeleteButtonOnClickListener(fragment, task));
  }

  private void hideButtons(View view) {
    View toolbar = view.findViewById(R.id.toolbar);
    if(toolbar.getVisibility() == View.VISIBLE) {
      ExpandAnimation expandAni = new ExpandAnimation(toolbar, 200);
      toolbar.startAnimation(expandAni);
    }
  }

  static class TaskViewHolder {
    @BindView(R.id.task_name) TextView name;
    @BindView(R.id.task_age) TextView age;

    TaskViewHolder(View view) {
      ButterKnife.bind(this, view);
    }
  }

  private class ListRowOnClickListener implements View.OnClickListener {

    @Override
    public void onClick(View view) {
      if(visiblePanel != null) {
        hideButtons(visiblePanel);
        visiblePanel = null;
      }
      View toolbar = view.findViewById(R.id.toolbar);
      ExpandAnimation expandAni = new ExpandAnimation(toolbar, 200);
      toolbar.startAnimation(expandAni);
      visiblePanel = toolbar;
    }
  }

  private class DeleteButtonOnClickListener implements View.OnClickListener, DialogModule.DialogDelegate {

    private TaskListFragment fragment;
    private Task task;

    DeleteButtonOnClickListener(TaskListFragment fragment, Task task) {
      this.fragment = fragment;
      this.task = task;
    }

    @Override
    public void onClick(View view) {
      DialogModule.showTaskDeleteDialog(fragment.getContext(), this);
    }

    @Override
    public void confirmTapped() {
      task.delete();
      fragment.refreshList();
    }

    @Override
    public void cancelTapped() { }
  }

  private class DoneButtonOnClickListener implements View.OnClickListener, DialogModule.DialogDelegate {

    private TaskListFragment fragment;
    private Task task;

    DoneButtonOnClickListener(TaskListFragment fragment, Task task) {
      this.fragment = fragment;
      this.task = task;
    }

    @Override
    public void onClick(View view) {
      DialogModule.showTaskDoneDialog(fragment.getContext(), this);
    }

    @Override
    public void confirmTapped() {
      task.complete();
      fragment.refreshList();
    }

    @Override
    public void cancelTapped() { }
  }
}
