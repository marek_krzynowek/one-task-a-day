package krzynowek.marek.onetaskaday.services;

import android.database.sqlite.SQLiteException;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import krzynowek.marek.onetaskaday.model.Task;

/**
 * Created by Marek Krzynowek on 12/26/16.
 */

public class TaskLotteryService {
  private static String TAG = TaskLotteryService.class.getName();

  //TODO: implement task ageing and queue to avoid duplicates
  public static @Nullable Task draw(){
    Random rGen = new Random();
    List<Task> tasks = new ArrayList<>();
    try {
      tasks = Task.getTodos();
    } catch (SQLiteException se) {
      Log.i(TAG, "draw: No tasks yet", se);
    }
    if(!tasks.isEmpty()) {
      Task current = Task.getCurrent();
      if (current != null) {
        tasks.remove(current);
        current.setCurrent(false);
        current.save();
      }
      int index = rGen.nextInt(tasks.size());
      Task task = tasks.get(index);
      task.setCurrent(true);
      task.save();
      return task;
    }
    return null;
  }
}
