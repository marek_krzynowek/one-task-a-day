package krzynowek.marek.onetaskaday.helpers;

import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.view.MotionEvent;
import android.view.View;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Marek Krzynowek on 12/28/16.
 */

public class OnSwipeListener implements View.OnTouchListener {

  private final static int THRESHOLD = 150;

  private @Nullable OnSwipeDelegate delegate;
  private float initialX;

  public static final String LEFT = "left_swipe";
  public static final String RIGHT = "right_swipe";
  @Retention(RetentionPolicy.SOURCE)
  @StringDef({ LEFT, RIGHT })
  public @interface Direction { }

  public OnSwipeListener(@Nullable OnSwipeDelegate delegate) {
    this.delegate = delegate;
  }

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    if(delegate == null) {
      return false;
    }

    if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
      initialX = event.getX();
      delegate.onTouch();
    } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
      float finalX = event.getX();
      if (initialX < finalX && finalX - initialX > THRESHOLD) {
        delegate.onSwipe(RIGHT);
      }
      if (initialX > finalX && initialX - finalX > THRESHOLD) {
        delegate.onSwipe(LEFT);
      }
      delegate.touchesDone();
    }
    return true;
  }

  public interface OnSwipeDelegate {
    void onTouch();
    void onSwipe(@Direction String direction);
    void touchesDone();
  }
}
