package krzynowek.marek.onetaskaday.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

import krzynowek.marek.onetaskaday.R;

/**
 * Created by marekkrzynowek on 05/01/2017.
 */

public class DialogModule {

  public static void showTaskDoneDialog(@NonNull Context context, @Nullable final DialogDelegate delegate ) {
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setMessage(R.string.confirm_dialog_message)
        .setTitle(R.string.confirm_dialog_title);
    builder.setPositiveButton(R.string.confirm_done, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if(delegate != null) {
          delegate.confirmTapped();
        }
      }
    });
    builder.setNegativeButton(R.string.not_done, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if(delegate != null) {
          delegate.cancelTapped();
        }
      }
    });
    AlertDialog dialog = builder.create();
    dialog.show();
  }

  public static void showTaskDeleteDialog(@NonNull Context context, @Nullable final DialogDelegate delegate ) {
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setMessage(R.string.confirm_delete_dialog_message)
        .setTitle(R.string.confirm_dialog_title);
    builder.setPositiveButton(R.string.confirm_delete, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if(delegate != null) {
          delegate.confirmTapped();
        }
      }
    });
    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if(delegate != null) {
          delegate.cancelTapped();
        }
      }
    });
    AlertDialog dialog = builder.create();
    dialog.show();
  }

  public interface DialogDelegate {
    void confirmTapped();
    void cancelTapped();
  }
}
